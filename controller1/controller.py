import controller_template as controller_template
import random
from math import floor

class Controller(controller_template.Controller):
    def __init__(self, track, evaluate=True, bot_type=None):
        super().__init__(track, evaluate=evaluate, bot_type=bot_type)

    #######################################################################
    ##### METHODS YOU NEED TO IMPLEMENT ###################################
    #######################################################################

    def normalize_feature(self, value, min, max, mode=0):
        if (mode): #normalize between [-1, 1]
            return (2 * (value - min) / (max - min)) - 1
        else: return (value - min) / (max - min) #normalize between [0, 1]
    
    def take_action(self, parameters: list) -> int:
        """  
        :param parameters: Current weights/parameters of your controller
        :return: An integer corresponding to an action:
        1 - Right 
        2 - Left
        3 - Accelerate 
        4 - Brake
        5 - Nothing
        """
        
         #Compute features
        features = self.compute_features(self.sensors)
        [f_Grass_Balance, f_Grass_Front, f_No_Obstacles, f_Obstacles_Close, f_Velocity, f_Enemy_Left, f_Enemy_Right] = [i for i in features]
        
        #Compute chances
        chance_turn_right = parameters[0] + parameters[1] * f_Grass_Balance + parameters[2] * f_Grass_Front + parameters[3] * f_Enemy_Left
        chance_turn_left = parameters[4] + parameters[5] * f_Grass_Balance + parameters[6] * f_Grass_Front + parameters[7] * f_Enemy_Right
        chance_accelerate = parameters[8] + parameters[9] * f_No_Obstacles + parameters[10] * abs(f_Grass_Balance)
        chance_brake = parameters[11] + parameters[12] * f_Obstacles_Close + parameters[13] * abs(f_Grass_Balance) + parameters[14] * f_Velocity
        chance_nothing = parameters[15] 
        
        chances = [chance_turn_right, chance_turn_left, chance_accelerate, chance_brake, chance_nothing]
        
        #return the maximum value of chances
        return chances.index(max(chances)) + 1

    def compute_features(self, sensors):   
        """        
        :param sensors: Car sensors at the current state s_t of the race/game
        contains (in order):        
            track_distance_left: 1-100                      
            track_distance_center: 1-100                    
            track_distance_right: 1-100                     
            on_track: 0 or 1                                
            checkpoint_distance: 0-???                      
            car_velocity: 10-200                            
            enemy_distance: -1 or 0-???                     
            position_angle: -180 to 180                     
            enemy_detected: 0 or 1  
          (see the specification file/manual for more details)
        :return: A list containing the features you defined 
        """

        #data gathering
        grass_left, grass_right, grass_front = sensors[0], sensors[2], sensors[1]
        on_track = sensors[3]
        checkp_dist, veloc = sensors[4], sensors[5]
        enemy_distance, enemy_angle, enemy_close = sensors[6], sensors[7], sensors[8]
        enemy_right, enemy_left, enemy_front = 0, 0, 0
        
        #other quantities
        no_near_enemies_front = 0
        grass_front_inverse = 100 - grass_front #from 0 to 99       
        
        if (90 > enemy_angle < 179) and (enemy_distance != -1):
          grass_balance = (grass_right - grass_left)/2
          no_near_enemies_front = 1
        elif (-179 > enemy_angle > -90) and (enemy_distance != -1):
          grass_balance = (grass_left - grass_right)/2
          no_near_enemies_front = 1
        else:
          no_near_enemies_front = 1
          grass_balance = grass_left - grass_right
        
        if enemy_distance > 100: enemy_distance = 100
        enemy_distance_inverse = 100 - enemy_distance
        
        if (0 > enemy_angle > -90) and (enemy_distance != -1): 
          enemy_left = enemy_distance_inverse
          no_near_enemies_front = 0
        elif (90 > enemy_angle > 0) and (enemy_distance != -1): 
          enemy_right = enemy_distance_inverse 
          no_near_enemies_front = 0    
        elif (enemy_angle == 0) and (enemy_distance != -1) and (grass_balance >= 0): enemy_right = enemy_distance_inverse
        elif (enemy_angle == 0) and (enemy_distance != -1) and (grass_balance < 0): enemy_left = enemy_distance_inverse 
        
        #Space features    
        f_Grass_Balance = self.normalize_feature(grass_balance, -99, 99, 1)
        f_Grass_Front = self.normalize_feature(grass_front, 1, 100)
        f_No_Obstacles = self.normalize_feature(no_near_enemies_front + f_Grass_Front, 0, 2)
        f_Grass_Front_Too_Close = self.normalize_feature(grass_front_inverse, 0, 99)
        f_Obstacles_Close = self.normalize_feature(f_Grass_Front_Too_Close, 0, 1)
        
        #Time and velocity features
        f_Velocity = self.normalize_feature(veloc, 10, 200)
        
        
        #Enemy features
        f_Enemy_Left = self.normalize_feature(enemy_left, 0, 100)
        f_Enemy_Right = self.normalize_feature(enemy_right, 0, 100)
        
        return [f_Grass_Balance, f_Grass_Front, f_No_Obstacles, f_Obstacles_Close, f_Velocity, f_Enemy_Left, f_Enemy_Right]

    #####################
    ### HILL CLIMBING ###
    #####################
        
    #This function was developed based on the pseudoalgorithms presented in class    
        
    def learn(self, weights) -> list:
    
        #adjustable values
        ALPHA = 0.01 #alpha value for generating neighbors
        NUM_NEIGHBORS = 60  #number of neighbors to be tested
        DEBUG = 1   #degug mode
    
        #function disturb
        #used to generate random neighbors of a state
        def disturb(list):
            list_size = len(list)
            
            #for each value of the state
            for index in range(0, list_size-1):
                rand_choice = random.randint(0, 2)
                if(rand_choice == 0): list[index] -= ALPHA #increment
                elif(rand_choice == 1): list[index] += ALPHA #decrement
                                                        #else keep current value
                
            return list    
        
        #generate random neighbors
        def gen_neighbors(weights, num):
            new_neighbors = []
            
            for x in range(0, num):
                new_weights = weights[:]   #copy weights
                disturbance = disturb(new_weights) #generate disturbed neighbors
                new_neighbors.append(disturbance) 
                
            return new_neighbors    

        ############    
        ### CORE ###
        ############
        
        #convert ndarray to list
        current_state = list(weights)
        
        while (1 == 1):
            neighbor_list = gen_neighbors(current_state, NUM_NEIGHBORS)
            
            best_neighbor = current_state
            
            for neighbor in neighbor_list:
                a = self.run_episode(neighbor)
                b = self.run_episode(best_neighbor)
                
                if(DEBUG): print ("best performance so far: ", b)
                
                if a > b:
                    best_neighbor = neighbor
                    
            if self.run_episode(best_neighbor) > self.run_episode(current_state):
                current_state = best_neighbor
            else:
                if (DEBUG): print (self.run_episode(best_neighbor)) #prints current value (debugging)
                return current_state
        
        
