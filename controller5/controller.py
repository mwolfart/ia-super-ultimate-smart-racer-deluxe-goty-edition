import controller_template as controller_template
import random
from math import floor

class Controller(controller_template.Controller):
    def __init__(self, track, evaluate=True, bot_type=None):
        super().__init__(track, evaluate=evaluate, bot_type=bot_type)

    #######################################################################
    ##### METHODS YOU NEED TO IMPLEMENT ###################################
    #######################################################################

    #normalizes a feature between [0,1] or [-1,1] (depending on which mode)
    def normalize_feature(self, value, min, max, mode=0):
        if (mode): #normalize between [-1, 1]
            return (2 * (value - min) / (max - min)) - 1
        else: return (value - min) / (max - min) #normalize between [0, 1]
    
    def take_action(self, parameters: list) -> int:
        """  
        :param parameters: Current weights/parameters of your controller
        :return: An integer corresponding to an action:
        1 - Right 
        2 - Left
        3 - Accelerate 
        4 - Brake
        5 - Nothing
        """
        
         #Compute features
        features = self.compute_features(self.sensors)
        [f_Grass_Balance, f_Grass_Front, f_No_Obstacles, f_Obstacles_Close, f_Velocity, f_Enemy_Left, f_Enemy_Right] = [i for i in features]
        
        #Compute chances
        chance_turn_right = parameters[0] + parameters[1] * f_Grass_Balance + parameters[2] * f_Grass_Front + parameters[3] * f_Enemy_Left
        chance_turn_left = parameters[4] + parameters[5] * f_Grass_Balance + parameters[6] * f_Grass_Front + parameters[7] * f_Enemy_Right
        chance_accelerate = parameters[8] + parameters[9] * f_No_Obstacles + parameters[10] * abs(f_Grass_Balance)
        chance_brake = parameters[11] + parameters[12] * f_Obstacles_Close + parameters[13] * abs(f_Grass_Balance) + parameters[14] * f_Velocity
        chance_nothing = parameters[15] 
        
        chances = [chance_turn_right, chance_turn_left, chance_accelerate, chance_brake, chance_nothing]
        
        #return the maximum value of chances
        return chances.index(max(chances)) + 1

    def compute_features(self, sensors):   
        """        
        :param sensors: Car sensors at the current state s_t of the race/game
        contains (in order):        
            track_distance_left: 1-100                      
            track_distance_center: 1-100                    
            track_distance_right: 1-100                     
            on_track: 0 or 1                                
            checkpoint_distance: 0-???                      
            car_velocity: 10-200                            
            enemy_distance: -1 or 0-???                     
            position_angle: -180 to 180                     
            enemy_detected: 0 or 1  
          (see the specification file/manual for more details)
        :return: A list containing the features you defined 
        """

        #data gathering
        grass_left, grass_right, grass_front = sensors[0], sensors[2], sensors[1]
        on_track = sensors[3]
        checkp_dist, veloc = sensors[4], sensors[5]
        enemy_distance, enemy_angle, enemy_close = sensors[6], sensors[7], sensors[8]
        enemy_right, enemy_left, enemy_front = 0, 0, 0
        
        #other quantities
        no_near_enemies_front = 0
        grass_front_inverse = 100 - grass_front #from 0 to 99       
        
        if (90 > enemy_angle < 179) and (enemy_distance != -1):
          grass_balance = (grass_right - grass_left)/2
          no_near_enemies_front = 1
        elif (-179 > enemy_angle > -90) and (enemy_distance != -1):
          grass_balance = (grass_left - grass_right)/2
          no_near_enemies_front = 1
        else:
          no_near_enemies_front = 1
          grass_balance = grass_left - grass_right
        
        if enemy_distance > 100: enemy_distance = 100
        enemy_distance_inverse = 100 - enemy_distance
        
        if (0 > enemy_angle > -90) and (enemy_distance != -1): 
          enemy_left = enemy_distance_inverse
          no_near_enemies_front = 0
        elif (90 > enemy_angle > 0) and (enemy_distance != -1): 
          enemy_right = enemy_distance_inverse 
          no_near_enemies_front = 0    
        elif (enemy_angle == 0) and (enemy_distance != -1) and (grass_balance >= 0): enemy_right = enemy_distance_inverse
        elif (enemy_angle == 0) and (enemy_distance != -1) and (grass_balance < 0): enemy_left = enemy_distance_inverse 
        
        #Space features    
        f_Grass_Balance = self.normalize_feature(grass_balance, -99, 99, 1)
        f_Grass_Front = self.normalize_feature(grass_front, 1, 100)
        f_No_Obstacles = self.normalize_feature(no_near_enemies_front + f_Grass_Front, 0, 2)
        f_Grass_Front_Too_Close = self.normalize_feature(grass_front_inverse, 0, 99)
        f_Obstacles_Close = self.normalize_feature(f_Grass_Front_Too_Close, 0, 1)
        
        #Time and velocity features
        f_Velocity = self.normalize_feature(veloc, 10, 200)
        
        
        #Enemy features
        f_Enemy_Left = self.normalize_feature(enemy_left, 0, 100)
        f_Enemy_Right = self.normalize_feature(enemy_right, 0, 100)
        
        return [f_Grass_Balance, f_Grass_Front, f_No_Obstacles, f_Obstacles_Close, f_Velocity, f_Enemy_Left, f_Enemy_Right]

    ########################
    ### HYBRID ALGORITHM ###
    ###########################################################
    ### This algorithm uses both genetic and beam approach. ###
    ###      It uses a genetic approach first to get a      ###
    ### sufficient high value,  and then uses beam approach ###
    ###       to find the local maximum of that value       ###
    ###########################################################

    def learn(self, weights) -> list:
    
        #adjustable values
        TIME_GENETIC = 150  #number of maximum iterations for genetic
        TIME_BEAM = 100     #number of maximum iterations for beam
        ALPHA = 0.001       #alpha value for causing disturbance in a state
        ALPHA_LIMIT = 0.1   #alpha value for generating random subjects (generates between these numbers)
        NUM_SOLUTIONS = 4   #number of solutions to be analysed and expanded (in both approaches)
        NUM_EXPANSIONS = 4  #number of expansions (neighbors) to be created from a solution
        NUM_FOREIGNERS = 3  #number of new random subjects to be added
        DEBUG = 1 #debug value (enables prints)
    
        #generate a random gene with given size
        #returns list of float
        def gen_random(size):
            genes = []
            
            while(size > 0):
                random_n = random.uniform(-ALPHA_LIMIT, ALPHA_LIMIT)
                genes.append(random_n)
                size -= 1
            
            return genes
            
        #generate given amount of random genes with given size
        #returns list of genes (a gene is a list of floats)
        def gen_random_n(size, n):
            individuals = []
            
            while(n > 0):
                individuals.append(gen_random(size))
                n -= 1
            
            return individuals
        
        #reproduce two parents to generate two children
        #returns list of genes (a gene is a list of floats)
        def reproduce(parent1, parent2):
            parent_size = len(parent1)
            half = floor(parent_size / 2)            
            
            child1 = parent1[:]
            child2 = parent1[:]
            
            child1[half:] = parent2[half:]
            child2[:half-1] = parent2[:half-1]
            
            return [child1, child2]
            
        #pick the N best states from a list of states
        #best states = the ones which run_episode has a higher value
        #returns list of states
        def pick_best(list_states, num):
            performances = []
            
            #save cost for each state
            for state in list_states:
                performances.append(self.run_episode(state))
            
            best = []
            first_performance = 1 # for debugging purposes
            
            while(num > 0):
                index = performances.index(max(performances)) #save index of higher performance
                best.append(list_states[index]) #append best state
                
                # for debugging purposes
                if first_performance and DEBUG:
                    first_performance = 0
                    print ("best performance so far: ", performances[index])
                
                #delete and end iteration
                del list_states[index]
                del performances[index]
                num -= 1
            
            return best  

        #function disturb
        #used to generate random neighbor of a state
        #returns list of floats
        def disturb(list):
            list_size = len(list)
            
            #for each value of the state
            for index in range(0, list_size-1):
                rand_choice = random.randint(0, 2)  #pick a random choice:
                if(rand_choice == 0): list[index] -= ALPHA #increment
                elif(rand_choice == 1): list[index] += ALPHA #decrement
                                                        #else keep current value
                
            return list    
        
        #generate given amount of random neighbors
        #returns list of states
        def gen_neighbors(weights, num):
            new_neighbors = []
            
            for x in range(0, num):
                new_weights = weights[:]            #copy weights
                disturbance = disturb(new_weights)  #generate disturbed neighbors
                new_neighbors.append(disturbance) 
                
            return new_neighbors  
    
        ############    
        ### CORE ###
        ############
        
        #convert ndarray to list
        current_state = list(weights)
        size = len(current_state)
        
        #use genetic approach to find sufficient good results
        if (DEBUG): print("starting genetic...")
        
        individuals = []
        individuals.append(current_state)
        
        #generate more random individuals
        individuals += gen_random_n(size, NUM_SOLUTIONS-1)     
        
        t=0 
        while(t < TIME_GENETIC):
            #generate children
            
            children1 = reproduce(individuals[0], individuals[1])
            children2 = reproduce(individuals[2], individuals[3])
            children3 = reproduce(individuals[1], individuals[2])
            
            population = individuals + children1 + children2 + children3
            population += gen_random_n(size, NUM_FOREIGNERS) #add more random individuals
            
            individuals = pick_best(population, NUM_SOLUTIONS)  #pick best
            t += 1
        
        #use beam approach to find local maximum
        if (DEBUG): print("starting beam...")
        
        state_set = individuals #rename variable (doesn't copy values, just sets another pointer)      
        t=0   
        while(t < TIME_BEAM):
            candidates = []
            candidates += state_set
        
            for state in state_set:
                candidates += gen_neighbors(state, NUM_EXPANSIONS)
        
            state_set = pick_best(candidates, NUM_SOLUTIONS)  #pick best
            t += 1

        return pick_best(state_set, 1)[0]   
        
        
